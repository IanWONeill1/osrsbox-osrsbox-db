{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "description": "A representation of an Old School RuneScape (OSRS) quest",

  "definitions": {
    "requirements.skills": {
      "type": "object",
      "required": [ "skill", "level", "boostable" ],
      "properties": {
        "skill": {
          "type": "string",
          "description": "The name of the required skill.",
          "enum": [ "attack", "strength", "defence", "ranged", "prayer", "magic", "runecrafting", "hitpoints", "crafting", "mining", "smithing", "fishing", "cooking", "firemaking", "woodcutting", "agility", "herblore", "thieving", "fletching", "slayer", "farming", "construction", "hunter" ]
        },
        "level": {
          "type": "integer",
          "description": "The skill level requirement to start a quest."
        },
        "boostable": {
          "type": "boolean",
          "description": "The boostability of a skill level to start a quest."
        }
      }
    },
    "rewards.skills": {
      "type": "object",
      "required": [ "skill", "xp" ],
      "properties": {
        "skill": {
          "type": "string",
          "description": "The name of the rewarded skill.",
          "enum": [ "attack", "strength", "defence", "ranged", "prayer", "magic", "runecrafting", "hitpoints", "crafting", "mining", "smithing", "fishing", "cooking", "firemaking", "woodcutting", "agility", "herblore", "thieving", "fletching", "slayer", "farming", "construction", "hunter" ]
        },
        "xp": {
          "type": "integer",
          "description": "The XP reward in a specific skill for completing the quest."
        }
      }
    }, 
    "items": {
      "type": "object",
      "required": [ "id", "quantity" ],
      "properties": {
        "id": {
          "type": "integer",
          "description": "The ID of the item required"
        },
        "quantity" : {
          "type": "integer",
          "description": "The quantity of an item requied"
        }
      }
    }
  },

  "title": "Quest Properties",
  "type": "object",
  "properties": {
    "id": {
      "type": "integer"
    },
    "name": {
      "type": "string"
    },
    "type": {
      "type": "string"
    },      
    "members": {
      "type": "boolean"
    },
    "release_date": {
      "type": "string",
      "format": "date"
    },
    "series": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "developers": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "start_point": {
      "type": "string"
    },
    "difficulty": {
      "type": "string"
    },
    "length": {
      "type": "string"
    },
    "description": {
      "type": "string"
    },
    "url_quest_image": {
      "type": "string",
      "format": "uri"
    },
    "url_quest_guide_full": {
      "type": "string",
      "format": "uri"
    },
    "url_quest_guide_quick": {
      "type": "string",
      "format": "uri"
    },
    "required_for_completing": {
      "type": "array",
      "items": {
        "type": "number"
      }
    },
    "quest_trivia": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "requirements": {
      "description": "Quest Requirements",
      "type": "object",
      "properties": {
        "quest_points": {
          "description": "The number of quest points required to start the quest.",
          "type": "integer"
        },
        "quests": {
          "description": "The quests that are required to start the quest",
          "type": "array",
          "items": {
            "type": "number"
          }
        },
        "skills": {
          "description": "The skill name, level and boostability required to start the quest.",
          "type": "array",
          "items": { "$ref": "#/definitions/requirements.skills" }
        },          
        "items_required": {
          "description": "The items and associated quantity required for the quest.",
          "type": "array",
          "items": { "$ref": "#/definitions/items" }
        },
        "items_recommended": {
          "description": "The items and associated quantity recommended for the quest.",
          "type": "array",
          "items": { "$ref": "#/definitions/items" }
        },
        "enemies_to_defeat": {
          "type": "array",
          "items": {
            "type": "number"
          }
        }
      },
      "required": [
          "quest_points",
          "quests",
          "skills",
          "items_required",
          "items_recommended",
          "enemies_to_defeat"
        ]
    },
    "rewards": {
      "description": "Quest Rewards",
      "type": "object",
      "properties": {
        "quest_points": {
          "description": "The quest points rewarded for completing the quest.",
          "type": "integer"
        },
        "coins": {
          "description": "The coins rewarded for completing the quest.",
          "type": "integer"
        },
        "items": {
          "description": "The items rewarded for completing the quest.",
          "type": "array",
          "items": { "$ref": "#/definitions/items" }
        },
        "skills": {
          "description": "The skill and XP rewards for completing the quest.",
          "type": "array",
          "items": { "$ref": "#/definitions/rewards.skills" }
        },
        "misc": {
          "title": "Quest Rewards Misc",
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      },
      "required": [
        "quest_points",
        "coins",
        "items",
        "skills",
        "misc"
      ]
    }
  },
  "required": [
    "id",
    "name",
    "type",
    "members",
    "release_date",
    "series",
    "developer",
    "start_point",
    "difficulty",
    "length",
    "description",
    "url_quest_image",
    "url_quest_guide_full",
    "url_quest_guide_quick",
    "required_for_completing",
    "quest_trivia",
    "requirements",
    "rewards"
  ]
}
